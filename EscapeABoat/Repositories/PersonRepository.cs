﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using EscapeABoat.Model;
using Newtonsoft.Json;
using NLog;

namespace EscapeABoat.Repositories
{
    internal static class PersonRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static Dictionary<int, Person> data;

        public static Dictionary<int, Person> GetPersons()
        {
            EnsureDataLoaded();

            return data;
        }

        private static void EnsureDataLoaded()
        {

            if (data != null)
            {
                return;
            }

            LoadData();
            EnrichData();
        }

        private static void LoadData()
        {
            var dataDirectoryRoot = Program.DataLocation.GetDirectories("persons").FirstOrDefault();
            if (dataDirectoryRoot == null)
            {
                Logger.Warn("Missing Persons directory.");
                data = new Dictionary<int, Person>();
                return;
            }

            Logger.Info($" Persons directory: {dataDirectoryRoot.FullName}");
            var dataFiles = dataDirectoryRoot.GetFiles();

            data = new Dictionary<int, Person>(dataFiles.Length);

            foreach (var dataFile in dataFiles)
            {
                var content = File.ReadAllText(dataFile.FullName);

                var fileData = JsonConvert.DeserializeObject<Person>(content);


                data.Add(fileData.Number, fileData);


            }
        }

        private static void EnrichData()
        {
            var nonsenseLogFile = Path.Combine(Program.DataLocation.FullName, "NonsenseLogs.txt");
            if (!File.Exists(nonsenseLogFile))
            {
                Logger.Warn("Missing nonsene log file.");
                return;
            }

            var lines = File.ReadLines(nonsenseLogFile).Where(s => !string.IsNullOrWhiteSpace(s)).ToArray();

            if (lines.Length <= 0)
            {
                Logger.Warn("No nonsense log messages defined.");
                return;
            }
           
            var logsToAddPerPassanger = lines.Length % data.Count;


            int i = 0;

            foreach (var person in data.Values)
            {
                var logsAdded = 0;

                do
                {
                    //free day
                    var day = -1;
                    do
                    {
                        day = Program.RND.Next(1, Program.VoyageLastLifeSign);
                    } while (person.Logs.Any(log => log.Day == day) &&
                             person.Logs.Count < Program.VoyageLastLifeSign - 5); //Noone writes logs every day

                    person.Logs.Add(new Log
                    {
                        Day = day,
                        Text = lines[i]
                    });

                    i++;
                    logsAdded++;

                    if (i >= lines.Length)
                    {
                        break;
                    }

                } while (logsAdded < logsToAddPerPassanger);

                if (i >= lines.Length)
                {
                    break;
                }
            }
        }
    }
}
