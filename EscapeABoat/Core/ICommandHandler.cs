﻿using System;

namespace EscapeABoat.Core
{
    public interface ICommandHandler
    {
        string Handle { get; }
        string[] Help { get; }
        ValueTuple<string, string>[] Usage { get; }
        bool Secret { get; }
        void Execute(string[] data);

    }
}
