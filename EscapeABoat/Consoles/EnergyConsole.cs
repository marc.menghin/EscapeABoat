﻿using EscapeABoat.Commands;
using EscapeABoat.Core;
using EscapeABoat.Helpers;

namespace EscapeABoat.Consoles
{
    internal sealed class EnergyConsole : ConsoleBase
    {
        public EnergyConsole()
        {
            RegisterCommand(new CloseCommand());
            RegisterCommand(new HelpCommand(commands));
            RegisterCommand(new EnergyManualCommand());
            RegisterCommand(new ClearCommand(this));
            RegisterCommand(new OpenCommand());
            RegisterCommand(new HistoryCommand(this));
        }

        public override void WriteHeader()
        {
            ConHelper.OutLine("Energy System Console");
            ConHelper.OutLineUrgent("--> Emergency Mode Activated <--");
            ConHelper.OutLine("");
            ConHelper.OutLineHint("For a list of commands type 'help' and hit the enter key!");
        }

        public override string GetInput()
        {
            var input = ConHelper.NextCommand("ShipIt-Energy");

            return input;
        }
    }
}
