﻿using EscapeABoat.Commands;
using EscapeABoat.Core;
using EscapeABoat.Helpers;

namespace EscapeABoat.Consoles
{
    internal sealed class CaptainsConsole : ConsoleBase
    {
       
        public CaptainsConsole() : base()
        {
            RegisterCommand(new ExitCommand());
            RegisterCommand(new HelpCommand(commands));
            RegisterCommand(new LogCommand());
            RegisterCommand(new PassangerCommand());
            RegisterCommand(new ShipsManualCommand());
            RegisterCommand(new ClearCommand(this));
            RegisterCommand(new OpenCommand());
            RegisterCommand(new HistoryCommand(this));
            RegisterCommand(new PingCommand());
        }

        public override void WriteHeader()
        {
            ConHelper.OutImportant("Ship: ");
            ConHelper.OutLine("Discovery II");

            ConHelper.OutImportant("Voyage Start: ");
            ConHelper.OutLine($"{Program.VoyageStart.ToShortDateString()} ({Program.VoyageDays} days ago)");

            ConHelper.OutImportant("Current Date: ");
            ConHelper.OutLine(Program.VoyageStart.AddDays(Program.VoyageDays).ToShortDateString());

            ConHelper.OutLine("Welcome Captain!");
            ConHelper.OutLineHint("For a list of commands type 'help' and hit the enter key!");
        }

        public override string GetInput()
        {
            var input = ConHelper.NextCommand("ShipIt-Captain");

            return input;
        }
    }
}
