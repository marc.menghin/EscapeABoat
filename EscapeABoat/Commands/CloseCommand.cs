﻿using System;
using EscapeABoat.Core;

namespace EscapeABoat.Commands
{
    public sealed class CloseCommand : ICommandHandler
    {
        public string Handle => "close";
        public string[] Help => new[] { "Close the terminal connection." };
        public ValueTuple<string, string>[] Usage => new[] { new ValueTuple<string, string>("close", null) };
        public bool Secret => false;

        public void Execute(string[] data)
        {
            Program.consoleService.CloseConsole();
        }
    }
}
