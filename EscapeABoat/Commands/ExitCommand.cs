﻿using System;
using EscapeABoat.Core;

namespace EscapeABoat.Commands
{
    public sealed class ExitCommand : ICommandHandler
    {
        public string Handle => "exit";
        public string[] Help => new[] { "Exit the game." };
        public ValueTuple<string, string>[] Usage => new[] { new ValueTuple<string, string>("exit", null) };
        public bool Secret => false;

        public void Execute(string[] data)
        {
            Program.consoleService.CloseConsole();
        }
    }
}
