﻿using System;
using EscapeABoat.Core;
using EscapeABoat.Helpers;

namespace EscapeABoat.Commands
{
    internal sealed class PingCommand : ICommandHandler
    {
        private readonly ConsoleBase _console;

        public string Handle => "ping";
        public string[] Help => new[] { "Play ping pong." };
        public ValueTuple<string, string>[] Usage => new[] { new ValueTuple<string, string>("ping", null) };
        public bool Secret => true;

        private long times;

        public PingCommand()
        {
            times = 0;
        }

        public void Execute(string[] data)
        {
            times++;

            if (times > 5)
            {
                if (times > 10)
                {
                    if (times > 15)
                    {
                        ConHelper.OutLineUrgent("PONG!");
                    }
                    else
                    {
                        ConHelper.OutLineImportant("PONG");
                    }
                }
                else
                {
                    ConHelper.OutLine("PONG");
                }
            }
            else
            {
                ConHelper.OutLine("pong");
            }
        }
    }
}
