﻿using System;
using System.Collections.Generic;
using System.Linq;
using EscapeABoat.Core;
using EscapeABoat.Helpers;

namespace EscapeABoat.Commands
{
    public sealed class HelpCommand : ICommandHandler
    {
        private Dictionary<string, ICommandHandler> _commands;

        public bool Secret => false;
        public string Handle => "help";
        public string[] Help => new[]
        {
            "Outputs the list of available commands.",
            "As well as detailes information for specific commands."
        };

        public ValueTuple<string, string>[] Usage => new[]
        {
            new ValueTuple<string, string>("help", "to list commands."),
            new ValueTuple<string, string>("help [command]", "to display help for the command")
        };

        public HelpCommand(Dictionary<string, ICommandHandler> commands)
        {
            _commands = commands;
        }

        public void Execute(string[] data)
        {
            if (data.Length <= 0)
            {
                ConHelper.OutLine("Note:");
                ConHelper.Out("Use '");
                ConHelper.OutHint("help [command]");
                ConHelper.OutLine("' to get more specific information.");
                ConHelper.OutLine("");
                ConHelper.OutLine("The commands are: ");
                foreach (var command in _commands.OrderBy(pair => pair.Key))
                {
                    if (command.Value.Secret)
                    {
#if DEBUG
                        ConHelper.OutUrgent("<SECRET>");
#else
                        continue;
#endif
                    }
                    ConHelper.OutImportant(command.Value.Handle);
                    ConHelper.OutLine(string.Format(" - {1}", command.Value.Handle, command.Value.Help.First()));
                }
            }
            else
            {
                ICommandHandler currentCommand;
                if (_commands.TryGetValue(data[0], out currentCommand))
                {

                    ConHelper.OutImportant("Command: ");
                    ConHelper.OutLine($"{currentCommand.Handle}");
                    ConHelper.OutLine("");
                    ConHelper.OutLineImportant("Usage:");
                    foreach (var usage in currentCommand.Usage)
                    {
                        ConHelper.CmdFormat();
                        Console.Write("<>: ");
                        ConHelper.OutHint($"{usage.Item1}");
                        if (usage.Item2 != null)
                        {
                            ConHelper.Out($" ({usage.Item2})");
                        }
                        ConHelper.OutLine("");
                    }
                    ConHelper.OutLine("");
                    foreach (var hlp in currentCommand.Help)
                    {
                        ConHelper.OutLine(hlp);
                    }
                }
                else
                {
                    ConHelper.OutLine($"The command '{data[0]}' is invalid.");
                }
            }


        }
    }
}
