﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EscapeABoat.Core;
using EscapeABoat.Helpers;
using EscapeABoat.Model;
using EscapeABoat.Repositories;

namespace EscapeABoat.Commands
{
    public sealed class LogCommand : ICommandHandler
    {
        public string Handle => "log";
        public bool Secret => false;

        public string[] Help => new[] { "Displays logs" };

        public ValueTuple<string, string>[] Usage => new[]
        {
            new ValueTuple<string, string>("log [passanger number]", "to list logs"),
            new ValueTuple<string, string>("log [passanger number] [log number]", "to show log")
        };

        private Dictionary<int, Person> persons;

        public LogCommand()
        {
            LoadData();
        }
        
        public void Execute(string[] data)
        {
            if (data.Length <= 0)
            {
                ConHelper.OutLine("Missing passanger number.");
            }
            else
            {
                int personId;
                if (!int.TryParse(data[0], out personId))
                {
                    ConHelper.OutLine("Couldn't read passanger number.");
                }
                else
                {
                    Person person;
                    if (persons.TryGetValue(personId, out person))
                    {
                        if (data.Length <= 1)
                        {
                            ConHelper.OutLine($"Log entries of {person.Name}:");
                            int i = 1;
                            foreach (var log in person.Logs.OrderBy(log => log.Day))
                            {
                                if (log.Text.Length > 30)
                                {
                                    ConHelper.OutLine($"  Nr.{i} [{log.Date.ToShortDateString()}] {log.Text.Substring(0, 30)}...");
                                }
                                else
                                {
                                    ConHelper.OutLine($"  Nr.{i} [{log.Date.ToShortDateString()}] {log.Text}");
                                }

                                i++;
                            }
                        }
                        else
                        {
                            int logNr;
                            if (!int.TryParse(data[1], out logNr))
                            {
                                ConHelper.OutLine($"{data[1]} is not a valid log number.");
                            }
                            else
                            {
                                if (logNr <= 0 ||  person.Logs.Count <= logNr-1)
                                {
                                    ConHelper.OutLineHint($"Log number {logNr} doesn't exist. Valid numbers are: 1-{person.Logs.Count}");
                                    return;
                                }
                                var logEntry = person.Logs[logNr - 1];
                                ConHelper.OutLine($"Log of: {person.Name}");
                                ConHelper.OutLine($"Entry Number: {logNr}");
                                ConHelper.OutLine($"Date: {logEntry.Date.ToShortDateString()}");
                                ConHelper.OutLine($"Text: {logEntry.Text}");
                            }
                        }
                    }
                    else
                    {
                        ConHelper.OutLine($"No Person with number {personId}");
                    }
                }
            }
        }

        private void LoadData()
        {
            persons = PersonRepository.GetPersons();
        }
    }
}
