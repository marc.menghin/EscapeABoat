﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EscapeABoat.Model
{
    internal sealed class Manual
    {
        public int Page { get; set; }
        public string Headline { get; set; }
        public string[] Text { get; set; }
    }
}
