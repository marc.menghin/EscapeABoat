﻿using System;
using System.IO;
using System.Reflection;
using EscapeABoat.Consoles;
using EscapeABoat.Core;
using EscapeABoat.Helpers;
using NLog;

namespace EscapeABoat
{
    public sealed class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        //information
        public static DirectoryInfo DataLocation;
        public static DateTime VoyageStart = new DateTime(2018, 04, 22);
        public static int VoyageDays = 29;
        public static int VoyageLastLifeSign = 25;

        //runtime stuff
        internal static Random RND;

        internal static ConsoleService consoleService;

        static void Main(string[] args)
        {

            Console.Title = "Escape A Boat v1.0 - Captains Terminal";
            ConHelper.OutHint("Initializing game data ... ");

            RND = new Random(DateTime.Now.Millisecond);
            consoleService = new ConsoleService();

            var gameDirectory = new DirectoryInfo(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            Logger.Info($"Game Directory: {gameDirectory.FullName}");

            DataLocation = new DirectoryInfo(Path.Combine(gameDirectory.FullName, "Resources"));
            Logger.Info($"Resource Directory: {DataLocation.FullName}");

            ConHelper.OutLineUrgent("DONE");

            consoleService.OpenConsole(new CaptainsConsole());

            while (consoleService.Current != null)
            {

                var input = consoleService.Current.GetInput();
                Logger.Info($"New Command: '{input}'");

                if (string.IsNullOrEmpty(input))
                {
                    continue;
                }

                consoleService.Current.ExecuteCommand(input);
            }

            
            ConHelper.OutLine("Bye! ..");

            ConHelper.OutLineHint("Press any key to close.");
            Console.ReadKey(); //wait for keypress
        }
    }
}
